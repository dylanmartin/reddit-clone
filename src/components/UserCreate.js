import React, { Component, Form, Modal, InputGroup } from 'react';

class Usercreate extends Component {

    render() {
        return (
        
                <Modal.Dialogue>
                    <Modal.Header closeButton>
                        Create Post Pros User
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group controlId="formBasicUsername">
                            <Form.Label>Username</Form.Label>
                            <Form.Control type="text" placeholder="Enter Username" />
                            <InputGroup className="inputBox" id="login-usernameInput" placeholder="username" />
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" />
                            <InputGroup className="pwInputBox" id="login-pwInput" />
                        </Form.Group>
                        <Form.Group controlId="formBasicConfirmPassword">
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control type="password" />
                            <InputGroup className="pwInputBox" id="login-pwInput" />
                        </Form.Group>
                    </Modal.Body>
                </Modal.Dialogue>

        )
    }
}

export default Usercreate;
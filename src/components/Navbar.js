
import React from 'react';
import { Navbar, Nav, NavItem, Form, FormControl, Button, NavDropdown } from 'react-bootstrap';
import { NavLink } from 'react-router-dom'
import Firebase from 'firebase'
import Login from './Login'
import '../css/Navbar.css'
import Signup from './Signup'
import Auth from './Auth'
export default class NavBar extends React.Component {
    constructor(props) {




        super(props);
        this.state = {
            app: Firebase.app(),
            authed: this.props.authed,
            db: Firebase.database(),
            categories: [],
            user:this.props.user
        }
        let ref = this.state.db.ref("/categories");
        
        const thiscomp = this;
        // Attach an asynchronous callback to read the data at our posts reference
        ref.on("value", function (snapshot) {
            console.log("stuff"+snapshot.val());
            thiscomp.setState({ categories: snapshot.val() });
        }, function (errorObject) {
            console.log("The read failed: " + errorObject.code);
        });
        
        console.log(this.state.db);
        this.filterCategory = this.filterCategory.bind(this);
        this.getCategories = this.getCategories.bind(this);
    }
    componentDidMount() {
        /*
        let ref = this.state.db.ref("categories");
        
        const thiscomp = this;
        // Attach an asynchronous callback to read the data at our posts reference
        ref.on("value", function (snapshot) {
            console.log("stuff"+snapshot.val());
            thiscomp.setState({ categories: snapshot.val() });
        }, function (errorObject) {
            console.log("The read failed: " + errorObject.code);
        });
        */
        

    }

    filterCategory = (event) => {
        let cat = event.target.innerHTML;
        this.props.setCat(cat);
    }

    getCategories = () => {
        

        
        let categories = [];
        let index = 0;
        
        categories.push(
            <NavItem onClick={(event) => this.filterCategory(event)} key={index} className="navdropdown">
                <NavDropdown.Item className="navdropdown">Any</NavDropdown.Item>
            </NavItem>
        );
        index++;
        for(let cat of this.state.categories) {
            categories.push(
                <NavItem onClick={(event) => this.filterCategory(event)} key={index} className="navdropdown">
                    <NavDropdown.Item className="navdropdown">{cat}</NavDropdown.Item>
                </NavItem>
            )
            index++;
        }
        return categories;
    }
    
    render() {
        return (
            //Render this if unauthed
            <Navbar className='navbar' expand='lg'>
                <div className="navLeft">

                    <NavItem>
                        <NavLink className='navItem' to='/'>
                            Home
                        </NavLink>
                    </NavItem>
                    <NavDropdown id='navDropdown' className='navItem navDropdown' title='Categories'>
                        {this.getCategories()}
                        {/* <NavItem className="navdropdown">
                            <NavDropdown.Item>Sports</NavDropdown.Item>
                        </NavItem>
                        <NavItem className="navdropdown">
                            <NavDropdown.Item>Politics</NavDropdown.Item>
                        </NavItem> */}
                    </NavDropdown>
                    <NavItem>
                        <NavLink className='navItem' to='/add'>
                            Add
                        </NavLink>
                    </NavItem>
                </div>
                <div className='navRight'>
                    
                    <Auth userPhoto={this.props.user[0].photoUrl} authed={this.props.authed} setAuth={this.props.setAuth} setAuthData={this.props.setAuthData} />
                        
                </div>
            </Navbar>
        );
    }

}
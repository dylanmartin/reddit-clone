import React from 'react';
import { Container, Form, FormLabel, InputGroup, Button, FormControl, Dropdown, DropdownButton, Col, Row } from 'react-bootstrap';
import '../css/AddPost.css';
import Firebase from 'firebase'
import Login from './Login'

export default class AddPost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            author: props.displayName,
            uid: props.uid,
            app: Firebase.app(),
            auth: Firebase.app().auth(),
            db: Firebase.database(),
            categories: []
        }
        this.uploadPost = this.uploadPost.bind(this);
        this.getCategories = this.getCategories.bind(this);
    }

    getCategories = () => {
        let categories = [];
        let index = 0;
        for (let cat of this.state.categories) {
            categories.push(
                <Dropdown.Item onClick={() => {
                    this.setState({ category: cat });
                }}
                    key={index}>
                    {cat}
                </Dropdown.Item>
            )
            index++;
        }
        return categories;
    }

    uploadPost = (auth) => {
        //adds the post to the firebase real time database
        document.getElementById('errorMessage').innerHTML = "";
        // check to see that all fields in the form are filled out
        if (!this.state.title) {
            document.getElementById('errorMessage').innerHTML = "Please fill out the title field."
            return;
        }
        if (!this.state.category) {
            document.getElementById('errorMessage').innerHTML = "Please fill out the category field."
            return;
        }
        if (!this.state.description) {
            document.getElementById('errorMessage').innerHTML = "Please fill out the description field."
            return;
        }
        // append data to the /posts ref of the database
        console.log('Uploading...');
        let ref = this.state.db.ref(`/posts`);
        console.log(JSON.stringify(auth,null,2));
        let post = {
            uId: auth.uid,
            author: auth.displayName,
            title: this.state.title,
            category: this.state.category,
            content: this.state.description
        }
        console.log(JSON.stringify(post,null,2))
        ref.push(post).then(
            () => {
                console.log('Done Uploading')
            }
        );
    }

    componentDidMount() {
        let ref = this.state.db.ref("/categories");
        const thiscomp = this;
        // Attach an asynchronous callback to read the data at our posts reference
        ref.on("value", function (snapshot) {
            thiscomp.setState({ categories: snapshot.val() });
        }, function (errorObject) {
            console.log("The read failed: " + errorObject.code);
        });
    }

    render() {
        if (!this.props.authed) {
            return (
                <Container >
                    <Row>
                        <Col className='authMessage mx-auto'>
                            please login to add a post
                    </Col>
                    </Row>
                </Container>
            )
        }
        return (

            <div>
                <h1>
                    Add Post
               </h1>
                <Container className="addContainer">
                    <Form>
                        <Form.Label>
                            Title
                        </Form.Label>
                        <FormControl
                            onChange={(event) => { this.setState({ title: event.target.value }) }}
                            id="postTitle"
                            placeholder="Somthing Witty"
                            aria-label="Title"
                        />
                        <Form.Label>
                            Category
                        </Form.Label>
                        <DropdownButton id="categortiesButton" title="none">
                            {this.getCategories()}
                        </DropdownButton>
                        <Form.Label>
                            Description
                        </Form.Label>
                        <FormControl
                            onChange={(event) => { this.setState({ description: event.target.value }) }}
                            id="addTextArea"
                            as="textarea"
                            aria-label="With textarea" />
                        <Button className="addButton" onClick={() => this.uploadPost(this.props.user[0])} title='Post'>
                            Post
                        </Button>
                    </Form>
                    <div id="errorMessage">

                    </div>
                </Container>
            </div>
        )
    }

}
import React, { Component } from 'react';
import { Button, Container, Col,Image } from 'react-bootstrap'
import Signup from './Signup'
import Login from './Login'
import Firebase from 'firebase'
import '../css/Auth.css'


class Auth extends Component {
    constructor(props) {
        super(props);
        this.state={
            authed:this.props.authed
        }
        
    }
    
    setShow=(val)=>{
        this.setState({show:val})
    }
    render() {
        if(this.props.authed==true){
            return(
                <div>
                    <Image src={this.props.userPhoto} style={{height:'3rem'}} roundedCircle></Image>
                </div>
            )
        }
        else{
            return (
                <div className="navRight">
                    <Signup setAuth={this.props.setAuth} setAuthData={this.props.setAuthData} />
                    <Login setUser={this.props.user} setAuth={this.props.setAuth} setAuthData={this.props.setAuthData} />     
                </div>
            );
        }
    }
}

export default Auth;
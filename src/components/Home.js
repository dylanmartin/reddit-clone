
import React from 'react';
import { Container, ListGroup, Row, Col, ListGroupItem, Button } from 'react-bootstrap';
import Post from './Post';
import Login from "./Login";
import '../css/Home.css';
// initialize firebase
import Firebase from 'firebase';
import Signup from './Signup';
import LoadingOverlay from 'react-loading-overlay';

let loading
export default class Home extends React.Component {
    constructor(props) {

        super(props);

        this.state = {
            userData: null,
            posts: [],
            app: Firebase.app(),
            auth: Firebase.app().auth(),
            db: Firebase.database(),
            showSignin: false,
        }
        loading=false;
    }


    renderCreateUser = () => {
        return (
            <Signup />
        );
    }
    renderPosts = () => {
        let posts = [];
        if (this.props.category !== 'Any') {
            for (let k of Object.keys(this.state.posts)) {
                let post = this.state.posts[k];
                if (post.category === this.props.category) {
                    posts.push(<Post auth={this.props.user} user={post.author} authed={this.props.authed} id={k} key={k} title={post.title} description={post.content} />);
                }
            }
        }
        else {
            for (let k of Object.keys(this.state.posts)) {
                let post = this.state.posts[k];
                posts.push(<Post auth={this.props.user} user={post.author} authed={this.props.authed} id={k} key={k} title={post.title} description={post.content} />);
            }
        }
        loading = false

        return posts;

    }
    renderLogin = () => {
        return (
            <Login />
        );
    }
    componentDidMount() {
        let ref = this.state.db.ref("/posts");
        const thiscomp = this;
        //Attach an asynchronous callback to read the data at our posts reference
        ref.on("value", function (snapshot) {
            thiscomp.setState({ posts: snapshot.val() });
        }, function (errorObject) {
            console.log("The read failed: " + errorObject.code);
        });
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.authed !== prevProps.authed) {
            if (this.state.authed === true) {
                console.log("we updated auth");
            }
        }
    }
    render() {
        return (
            /* <LoadingOverlay
                active={loading}
                spinner
                text='Loading your content...'
                styles={{
                    wrapper: {
                        width: document.width,
                        height: document.height,
                        overflow: loading ? 'hidden' : 'scroll'
                      }
                }}
            >*/
                <div className='homeBody'>
                    <h1 className='title'>
                        <Container>
                            <Row>
                                <Col className="mx-auto" >
                                    Welcome to Post Pros
                            </Col>
                            </Row>
                        </Container>
                    </h1>
                    <Container>
                        <ListGroup>
                            {this.renderPosts().length === 0 ? <h3 style={{ width: '100%', margin: 'auto', textAlign: 'center' }}>Sorry but there are no posts for this category :(</h3> : this.renderPosts()}
                        </ListGroup>
                    </Container>

                </div>
            //</LoadingOverlay>

        )
    }

}
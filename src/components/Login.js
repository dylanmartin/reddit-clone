
import React, { Component } from 'react';
import { InputGroup, Form, Button, Container, Modal, ModalDialog } from 'react-bootstrap';
import '../css/Login.css';
import Firebase from 'firebase';


class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            show: false
        }
        this.login = this.login.bind(this);
    }



    login = () => {

    }



    render() {
        return (
            <div>
                <Button className='loginButton' onClick={()=>{this.setState({show:true})}}>Login</Button>
                <Modal show={this.state.show}
                    onHide={() => {this.setState({show:true})}}
                >
                    <ModalDialog className="loginContainer">

                        <Modal.Header className='loginContainer' closeButton>
                            Login
                    </Modal.Header>
                    <Modal.Body className='loginContainer'>
                        <Form.Group  className="loginContainer" controlId="formBasicUsername">
                            <Form.Label>Username</Form.Label>
                            <Form.Control type="text" placeholder="Enter Username" />
                            <InputGroup className="inputBox" id="login-usernameInput" placeholder="username" />
                        </Form.Group>
                        <Form.Group className="loginContainer" controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" />
                            <InputGroup className="pwInputBox" id="login-pwInput" />
                        </Form.Group>

                        <Button className="loginGoogleButton" onClick={() => {
                            var provider = new Firebase.auth.GoogleAuthProvider();


                            let that = this;
                            Firebase.auth().signInWithPopup(provider).then(function (result) {
                                // this.props.authed ({
                                //     username:result.username,
                                //     authCredential:result.credential
                                // });
                                that.props.setAuth(true);
                                that.props.setAuthData(result);
                                that.setState({show:false});

                            }, function (error) {
                                return false;
                            });
                        }} variant="success" class="" >Login With Google</Button>
                        </Modal.Body>
                        <Modal.Footer className="loginContainer">
                            <Form.Group>
                                {/* <Button variant="secondary" onClick={() => {
                                    this.state.show = false;
                                }}>Close</Button> */}
                                <Button className="loginGoogleButton" variant="primary" onClick={() => {


                                    //Firebase.auth().createUserWithEmailAndPassword();

                                    alert(1);
                                    //this.props.setAuthData( )

                                }}

                                >Sign Up</Button>
                            </Form.Group>
                        </Modal.Footer>
                    </ModalDialog>
                </Modal>
            </div>

        )
    }
}


export default Login;
import React from 'react';
import { ListGroupItem, FormControl, Button } from 'react-bootstrap';
import '../css/Post.css';
import Firebase from 'firebase'
export default class Post extends React.Component {
    constructor(props) {
        super(props);
        console.log("started");
        this.state = {
            app: Firebase.app(),
            auth: Firebase.app().auth(),
            db: Firebase.database(),
        }
        this.addComment = this.addComment.bind(this);
        this.writeComment = this.writeComment.bind(this);
        this.renderComments = this.renderComments.bind(this);
    }

    addComment = (event) => {
        console.log(this.props.authed);
        if(!this.props.authed) {
            alert("You must be logged in to write a comment!");
            return;
        }
        document.getElementById(`commentContainer${this.props.id}`).style.display = 'none';
        // TODO make sure user is authed in and if not redirect to logoin page
        let ref = this.state.db.ref(`/posts/${this.props.id}`);
        let comment = {
            comment:this.state.comment,
            author: this.props.auth[0].displayName
        };
        console.log(JSON.stringify(comment, null, 2));
        ref.child('comments').push(comment).then(
            () => {
                console.log('Done Uploading')
                let ref = this.state.db.ref(`/posts/${this.props.id}/comments`);
                const thiscomp = this;
                //Attach an asynchronous callback to read the data at our posts reference
                ref.on("value", function (snapshot) {
                    thiscomp.setState({ comments: snapshot.val() });
                }, function (errorObject) {
                    console.log("The read failed: " + errorObject.code);
                });
            }
        );
    }

    showComments = () => {
        let c = document.getElementById(`comments${this.props.id}`);
        if(c.style.display === 'none') {
            c.style.display = 'block';
        }
        else{
            c.style.display = 'none';
        }
    }

    writeComment = () => {
        document.getElementById(`commentContainer${this.props.id}`).style.display = 'block';

    }

    renderComments = () => {
        let comments = []
        if (this.state.comments) {
            for (let k of Object.keys(this.state.comments)) {
                // add author field or somthing
                comments.push(
                    <div className="commentListItem" key={k}>
                        <span style={{padding:"1%"}}>
                            {this.state.comments[k].author}
                        </span>
                        <pre style={{padding:"1%",marginLeft:"5%"}}>
                            {this.state.comments[k].comment}
                        </pre>
                    </div>
                );
            }
        }
        return comments;
    }

    componentDidMount() {
        let ref = this.state.db.ref(`/posts/${this.props.id}/comments`);
        const thiscomp = this;
        //Attach an asynchronous callback to read the data at our posts reference
        ref.on("value", function (snapshot) {
            thiscomp.setState({ comments: snapshot.val() });
        }, function (errorObject) {
            console.log("The read failed: " + errorObject.code);
        });
    }
    render() {
        return (
            <ListGroupItem className='postContainer'>
                <h3 className='postTitle'>
                    {this.props.title}
                </h3>
                <br />
                <div>
                    Created By: {this.props.user}
                </div>
                <br />
                <div className='postDescription'>
                    <pre>
                        {this.props.description}
                    </pre>
                </div>
                <div onClick={() => this.showComments()} className="commentButton">
                    <img className='commentImg' src="./iconfinder_06_111027.svg" />
                    <span>
                        Comments
                    </span>
                </div>
                <div id={`comments${this.props.id}`} className='comments'>
                    {this.renderComments()}
                    <Button className='postButton' onClick={() => this.writeComment()}>
                        Add
                    </Button>
                </div>
                <div id={`commentContainer${this.props.id}`} className="commentContainer">
                    <FormControl
                        onChange={(event) => { this.setState({ comment: event.target.value }) }}
                        id="addTextArea"
                        as="textarea"
                        aria-label="With textarea" />
                    <Button className='postButton' onClick={() => this.addComment()}>
                        Comment
                    </Button>
                    <Button className='postButton' onClick={() =>  document.getElementById(`commentContainer${this.props.id}`).style.display = 'none'}>
                        Cancel
                    </Button>
                </div>
            </ListGroupItem>
        );
    }
}
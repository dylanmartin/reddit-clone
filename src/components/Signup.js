import React from 'react'
import { Form, Modal, Button, InputGroup } from 'react-bootstrap'
import Firebase from 'firebase';
import "../css/Signup.css"


class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      username: "",
      email: "",
      token: ""
    }

  }
  setShow = (val) => {
    this.setState({ show: val });
  }


  render() {
    return (
      <div>
        <Button className="signupButton" onClick={() => { this.setShow(true) }} >
          Sign Up

        </Button>
        
          <Modal className='signupContainer'

            show={this.state.show}
            onHide={() => { this.setShow(false) }}
          >

            <Modal.Dialog>
              <Modal.Header className="signupFormContainer" closeButton>
                Create Post Pros User
                    </Modal.Header>
              <Modal.Body className="signupFormContainer">
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Email</Form.Label>
                  <Form.Control type="text" placeholder="Enter Email" />
                  <InputGroup className="inputBox" id="login-emailnput" placeholder="username" />
                </Form.Group>
                <Form.Group controlId="formBasicUsername">
                  <Form.Label>Username</Form.Label>
                  <Form.Control type="text" placeholder="Enter Username" />
                  <InputGroup className="inputBox" id="login-usernameInput" placeholder="username" />
                </Form.Group>
                <Form.Group controlId="formBasicPassword">
                  <Form.Label>Password</Form.Label>
                  <Form.Control type="password" />
                  <InputGroup className="pwInputBox" id="login-pwInput" />
                </Form.Group>
                <Form.Group controlId="formBasicConfirmPassword">
                  <Form.Label>Confirm Password</Form.Label>
                  <Form.Control type="password" />
                  <InputGroup className="pwInputBox" id="login-pwInput" />
                </Form.Group>
              </Modal.Body>
              <Modal.Footer className="signupFormContainer">
                <Form.Group>
                  {/* <Button variant="secondary" onCLick={() => {
                    this.state.show = false;
                  }}>Close</Button> */}
                  <Button className='createButton' onClick={() => {
                  

                      //Firebase.auth().createUserWithEmailAndPassword();

                      alert(1);
                      //this.props.setAuthData( )
                    
                  }}
                  
                  >Sign Up</Button>
                </Form.Group>
              </Modal.Footer>
            </Modal.Dialog>
          </Modal>
        
      </div>
    )
  }
}

export default Signup;


import React from 'react';
import './App.css';
import Home from './components/Home';
import NavBar from './components/Navbar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import AddPost from './components/AddPost';
import Login from './components/Login';
import Account from './components/Account'
import Firebase from 'firebase'


// CSS Color Scheme
// Redish #CF6766
// Navvy #30415D
// Black #031424
// Lightblue #8EAEBD


const firebaseConfig = {
  apiKey: "AIzaSyCzAftBiFfk0kNWUDcZ21Ni5ocPEfPMu-I",
  authDomain: "redditclone-326c0.firebaseapp.com",
  databaseURL: "https://redditclone-326c0.firebaseio.com/",
  projectId: "redditclone-326c0",
  storageBucket: "redditclone-326c0.appspot.com",
  messagingSenderId: "595132816444",
  appId: "1:595132816444:web:fc93475e3e82b5667b7739",
  measurementId: "G-FLC7YB5HYP"
};
const app = Firebase.initializeApp(firebaseConfig);

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      authed: false,
      userAuth:[{
        displayName:"",
        email:"",
        photoUrl:"",
      }],
      db:Firebase.database(app),
      category:'Any'
    }
    console.log(this.state.db.ref());

    //this.setSignInShow = this.setSignInShow.bind(this);
    this.setAuth = this.setAuth.bind(this);
    this.setAuthData = this.setAuthData.bind(this);
    this.setCat = this.setCat.bind(this);
  }
  setCat = (cat) => {
    this.setState({category:cat})
  }

  // setSignInShow = (val) => {
  //   this.setState({ showSignIn: val });
  // }
  setAuth = (auth) => {
    console.log(this.state.auth);
    this.setState({ authed: auth });
  }
  setAuthData = (user)=> {
   console.log(user);
    this.setState(
    {
      userAuth:[{
        uid: user.user.uid,
        displayName:user.user.displayName,
        email:user.user.email,
        photoUrl:user.user.photoURL
      }]
    }

    )
    


  }
  componentDidMount() {
    

}

  // loginSetState = (loginInfo) => {
  //   this.setState({ userData: loginInfo });
  // }


  render() {
    return (
      <Router>
             
        {
          <div>
            <NavBar setCat={this.setCat} user={this.state.userAuth} setAuth={this.setAuth} setAuthData={this.setAuthData} authed={this.state.authed} />
            <Switch>
              <Route exact path='/'  config={firebaseConfig} component={() => <Home category={this.state.category} setAuth={this.setAuth} setAuthData={this.setAuthData} authed={this.state.authed} user={this.state.userAuth} />} />
              <Route path='/add' component={() => <AddPost setAuth={this.setAuth} setAuthData={this.setAuthData} authed={this.state.authed} user={this.state.userAuth} />} />
              <Route path='/login' component={Login} />
              <Route path='/account' user={this.state.userAuth} component={Account}/>
            </Switch>
          </div>

        }

      </Router>
    );
  }
}

